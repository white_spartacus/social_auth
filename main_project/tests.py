from django.test import TestCase
from django.test import Client

from .forms import *


class TestForm(TestCase):
    def setUp(self):
        self.form_data = {
            'first_name': 'name1',
            'last_name': 'name1',
            'email': '123123@ya.ru',
            'password': '123',
            'confirm_password': '123',
            'license_': True
        }

    def test_form_valid(self):
        form = FormAddUser(data=self.form_data)
        self.assertTrue(form.is_valid())
        self.assertTrue(form.cleaned_data['license_'])
        comment = form.save()
        self.assertEqual('name1', comment.first_name)
        self.assertEqual('name1', comment.last_name)
        self.assertEqual('123123@ya.ru', comment.email)
        self.assertEqual('123', comment.password)

    def test_form_blank(self):
        form = FormAddUser(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'password': ['This field is required.'],
            'confirm_password': ['This field is required.'],
            'license_': ['This field is required.'],
            'last_name': ['This field is required.'],
        })

    def test_send_form(self):
        c = Client()
        resp = c.post('/user/add/', {})
        self.assertEqual(resp.status_code, 200)
        self.assertFormError(resp, 'form', 'first_name', 'This field is required.')
        self.assertFormError(resp, 'form', 'last_name', 'This field is required.')
        self.assertFormError(resp, 'form', 'email', 'This field is required.')
        self.assertFormError(resp, 'form', 'password', 'This field is required.')
        self.assertFormError(resp, 'form', 'confirm_password', 'This field is required.')
        self.assertFormError(resp, 'form', 'license_', 'This field is required.')
        resp = c.post('/user/add', self.form_data, follow=True)
        self.assertEqual(resp.status_code, 200)

