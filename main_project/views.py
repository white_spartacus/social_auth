from django.views.generic import FormView, RedirectView

from .forms import FormAddUser


class Index(RedirectView):
    permanent = False
    query_string = True
    pattern_name = 'user-add'

    def get_redirect_url(self, *args, **kwargs):
        return super().get_redirect_url(*args, **kwargs)


class RegApp(FormView):
    form_class = FormAddUser
    template_name = 'main_project/index.html'
    success_url = '/home/'

    def form_valid(self, form):
        from django.contrib.auth import authenticate, login
        email = form.cleaned_data['email']
        form.instance.username = email.split('@')[0]
        form.save()
        user = form.instance
        login(
            self.request,
            user,
            backend='django.contrib.auth.backends.ModelBackend'
        )
        return super(RegApp, self).form_valid(form)
