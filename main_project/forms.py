from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django import forms


class FormAddUser(forms.ModelForm):
    """
    Форма для локального добавления
    клиента
    """
    first_name = forms.CharField(widget=forms.TextInput(
                    attrs={
                        'placeholder': 'First Name', 
                        'class': 'form-control' 
                    }))
    last_name = forms.CharField(widget=forms.TextInput(
                    attrs={
                        'placeholder': 'Last Name',
                        'class': 'form-control' 
                    }))
    email = forms.EmailField(required=True, widget=forms.TextInput(
                    attrs={
                        'placeholder': 'Email',
                        'class': 'form-control' 
                    }))
    password = forms.CharField(widget=forms.PasswordInput(
                    attrs={
                        'placeholder': 'Password',
                        'class': 'form-control'
                    }))
    confirm_password = forms.CharField(widget=forms.PasswordInput(
                    attrs={
                        'placeholder': 'Confirm Password',
                        'class': 'form-control'
                    }))
    license_ = forms.BooleanField(
                    help_text='I\'d  like to receive PlacePass news and offers',
                    required=True
                    )
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password',
            'confirm_password'
        )

    def clean_confirm_password(self):
        pswd = self.cleaned_data['confirm_password']
        if pswd != self.cleaned_data.get('password'):
            raise forms.ValidationError('Password not confirmed')
        return pswd

    def clean_license_(self):
        lc = self.cleaned_data['license_']
        if not lc: 
            raise forms.ValidationError('Don\'t enter checkbox like to receive')
        return lc

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email):
            raise forms.ValidationError('Email is use')
        return email
        
