from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from .views import *


urlpatterns = [
    path('', Index.as_view()),
    path('user/add/', RegApp.as_view(), name='user-add'),
    path('home/', TemplateView.as_view(template_name='main_project/home.html'), name='home'),
]
